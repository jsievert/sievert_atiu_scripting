﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battler : MonoBehaviour
{
    //health
    int health;
    //damage on hit
    int strength;
    //chance to dodge an attack
    int speed;
    //enemy/player name
    string name;
    //tracks whether dead or not
    bool alive = true;

    //replace this will the type you can override
    void Attack()
    {
        //will be slightly different for player, so we can use our override
    }

    // replace this with the type you can override
    void Damaged(int damage)
    {
        //should be same for all classes
        health = health - damage;
        string message = name + " has " + health + "hit points remaining";
    }
    //set up getters and setters for health, strength and speed
    //
    //
    //
    //
    void Attacked(int damage)
    {
        //randomint (1-100)
        int dodgeCheck = 25;
        string message = "";
        message = dodgeCheck <= speed ? name + " was hit." : name + " dodged the attack!";
        //output function in Controller that prints anything fed to it. Controller.Output(Message);
        if (dodgeCheck > speed)
        {
            Damaged(damage)
        }
    }
}
